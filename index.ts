import express, { Express, Response, Request } from "express";
import dotenv from "dotenv";
// .Env configuration
dotenv.config();

// Express app
const app: Express = express();
const port: string | number = process.env.PORT || 3001;

// Execute Express Server
app.listen(port, () => {
  console.log(`Listening port ${port}`);
});
//Express Routes
app.get("/", (req: Request, res: Response) => {
  const { name } = req.query;
  name
    ? res.send({
        data: {
          message: `Hello ${name}`,
        },
      })
    : res.send({
        data: {
          message: "Goodbye, world",
        },
      });
});
